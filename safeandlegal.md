# Being safe and legal online

The skills required to stay safe, legal and confident online.

## Skills for life and work

I can:

* respond to requests for authentication for my online accounts and email
* keep the information I use to access my online accounts secure, using different and secure passwords for websites and accounts
* set privacy settings on my social media and other accounts
* identify secure websites by looking for the padlock and https in the address bar
* recognise suspicious links in email, websites, social media messages and pop ups and know that clicking on these links or downloading unfamiliar attachments could put me and my computer at risk
* make sure that any information or content is backed up frequently by making a copy and storing it separately either in the cloud or on an external storage device

## Skills for life examples

I can:

1. make sure that online login information is not shared with anyone
2. ensure your posts on social media are not offensive or inappropriate
3. ensure that nothing is posted on social media about others, including children, without their permission
4. use a second device to receive codes when a website provides dual factor authentication and input the code to access the associated account
5. create passwords using three random words or with at least 8 characters, using lower- and upper-case letters, numbers and symbols
6. apply privacy settings to Facebook to ensure only friends can see posts and shared content
7. activate pop-up blockers on my web browser to reduce the threat from malicious sites
8. set automatic updates in the settings menu for the computer operating system and security software
9. use search tools to find and access images and other online content that can be used by others
10. use an external storage drive and copy any new documents on to it at the end of the day

The information presented above is done so under the conditions set out on the [source](./README.md) page

<hr>

How we can do this with; Free Software, Open source software and privacy friendly websites.


1. Password managers
 * Browsers have built in password Managers
2. Tech pledge
 * [Tech Pledge](https://www.techpledge.org/)
 #nsfw
   * Content on Mastodon etc can be marked with #nsfw. You can also hide ALL images, so users are able to click to view only.
3. * Seek permission from anyone (not just children and young people) before taking pictures / video.
   * Under 18's cannot give consent so you need to ask legal parent / guardian (this should be written down)
   * Be open / transparent as to where photos are shared, e.g facebook, some people may object to certain websites
   * [General Data Protection Regulation (GDPR)](https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/)
4. 2FA (Two Factor Authentication) - Free/Libre/Open Source solutions
   * [Free/Libre/Open Source Two Factor Authentication]
5. `apg`
   * GNU/Linux tool to help produce secure passwords
6. Mastodon is a alternative to Facebook, it has privacy settings etc.
7. Pop up blockers
8. auto updates
    * GNU/Linux distribution have their own update methods, use these to keep your system up to date.
9. DuckDuckGo / Creative Commons / [open clipart](https://openclipart.org/),
    * Ensure any media downloaded is creative commons (or under other free licenses) for example, attribute to creators and avoid nonfree material when and where possible
10. mounting and nextcloud
    * External storage can be auto mounted to gain access, or set to be mounted manually, you can use a web browser to access nextcloud or use a desktop client or the file manager to move files to / from the cloud.

Creative Commons is a way to license work for reuse. People can then use within the license without asking for permission in general need to attribute the work to the creator.

1. [UK Copyright](https://www.gov.uk/guidance/changes-to-copyright-law-after-the-transition-period?utm_campaign=transition_p2&utm_medium=cpc&utm_source=seg&utm_content=act29&gclid=CInipYH01e0CFdOTGwodEbgH0A)
2. [Creative Commons](https://creativecommons.org/)
3. [Tech Pledge](https://www.techpledge.org/)
   * Use the #NSFW tag for sensitive content on Decentralised social media.
4. Passwords<br/>
   One (secure) method of generating passwords:
   * At least 8 characters
   * A mix of:
     * Upper case letters
     * Lower case letters
     * Numbers
     * Special characters e.g `$`, `%`, `^`<br/>
   Under Debian, software such as `apg` can generate good passwords for you.
   `apg  -M SNCL -m 16 -n 5`, for example produces: `CloHensyiv7Ojof!`.
   You can also use the [Diceware method](https://en.wikipedia.org/wiki/Diceware).
   Your computer can generate Diceware passwords with the `diceware` program.

4. [Free Software Licenses GNU](https://www.gnu.org/)
5. [Digital Detox](https://datadetoxkit.org/en/home/)
6. [DuckDuckGo](https://www.duckduckgo.com)
7. [Mastodon Privacy](https://docs.framasoft.org/en/mastodon/User-guide.html)

[Digital Restrictions Management](https://en.wikipedia.org/wiki/Digital_rights_management) is a way to control how you use media, software, books etc. It is designed to counter copyright infringement (but is ineffective as there is always a way round it). However, in doing so, it also restricts how you can use legally bought material. You can't copy material to another device, make backups or if you buy a new device, in some cases, copy to the new device.

There is a counter argument from the [Free Software Foundation](https://www.defectivebydesign.org/) as DRM is not really very good at doing this.

